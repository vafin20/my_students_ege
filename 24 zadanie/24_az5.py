f = open("24 (20).txt")

a = f.read().splitlines()

str = ""
min = 1000000
for i in range(0, len(a)):
    if a[i].count("N") < min:
        min = a[i].count("N")
        str = a[i]

count = []

for i in range(26):
    count.append(0)

for i in range(len(str)):
    index = ord(str[i]) - ord("A")
    count[index] += 1

max = 0
max_index = 0

for i in range(len(count)):
    if count[i] >= max:
        max = count[i]
        max_index = i

print(chr(max_index + ord("A")))