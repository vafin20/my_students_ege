f = open("inf_26_04_21_24 (1).txt")

a = f.read().splitlines()

strings = []
max_dist = -1

for string in a:
    if string.count("A") < 25:
        strings.append(string)

for string in strings:
    for i in range(len(string)):
        if string.count(string[i]) > 1:
            dist = string.rfind(string[i]) - i
            if dist > max_dist:
                max_dist = dist

print(max_dist)