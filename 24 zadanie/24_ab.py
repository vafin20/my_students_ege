'''
последовательность AB
ABABACBABAA
12345001231
'''

f = open("")

a = f.read()
count = 0
max = -1
for i in range(0, len(a)):
    if a[i] == "A" and count % 2 == 0 or a[i] == "B" and count % 2 == 1:
        count += 1
    elif a[i] == "A":
        count = 1
    else:
        count = 0
print(count)
