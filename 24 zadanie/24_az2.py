f = open("24 (14).txt")

a = f.read()

count = []

for i in range(26):
    count.append(0)

for i in range(len(a) - 2):
    if a[i] == a[i + 2]:
        index = ord(a[i + 1]) - ord("A")
        count[index] += 1

max = 0
max_index = 0

for i in range(len(count)):
    if count[i] > max:
        max = count[i]
        max_index = i

print(chr(max_index + ord("A")))