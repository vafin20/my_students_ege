f = open("24_demo.txt")

a = f.read()

count = 0
count_max = 0

for x in a:
    if (x == "X" and count % 3 == 0) or (x == "Y" and count % 3 == 1) or (x == "Z" and count % 3 == 2):
        count += 1
        if count > count_max:
            count_max = count
    elif x == "X":
        count = 1
    else:
        count = 0
print(count_max)