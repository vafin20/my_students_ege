'''
Определите символ, который чаще всего встречается в файле МЕЖДУ двух одинаковых символов.
'''

f = open("24 (11).txt")

a = f.read()

count = []

for i in range(26):
    count.append(0)

for i in range(1, len(a) - 1):
    if a[i - 1] == a[i + 1]:
        index = ord(a[i]) - ord("A")
        count[index] += 1

max_index = 0
max_count = 0

for i in range(len(count)):
    if count[i] > max_count:
        max_count = count[i]
        max_index = i


symbol = chr(max_index + ord("A"))

print(symbol)