f = open("24 (23).txt")

a = f.read().split("A")

max_sum = 0

for i in range(len(a) - 1):
    sum = len(a[i]) + len(a[i + 1]) + 1
    if sum > max_sum:
        max_sum = sum

print(max_sum)