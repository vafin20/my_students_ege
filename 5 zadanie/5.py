
a = [0 for i in range(1000)]

a[0] = 0
a[1] = 2
a[2] = 9

for i in range(3, 100):
    a[i] = i + 1 + 2*i + a[i - 1] + a[i - 3]

for i in range(len(a)):
    if a[i] > 1_000_000:
        print(i)