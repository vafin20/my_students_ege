f = open()
a = f.read().splitlines()[1:]

week_start = 1633305600
week_end = 1633305600 + 604800

time = [0 for i in range(604800)]
count = 0

for x in a:
    start, stop = map(int, x.split())
    if start < week_start and (stop > week_start or stop == 0):
        count += 1
    if start >= week_start and stop <= week_end:
        time[start - week_start] += 1
    if week_start <= stop <= week_end:
        time[stop - week_start] -= 1

max_len = 0
max_sum = 0

for x in time:
    count += x
    if count > max_sum:
        max_sum = count
        max_len = 0
    if count == max_sum:
        max_len += 1

print(max_sum, max_len)