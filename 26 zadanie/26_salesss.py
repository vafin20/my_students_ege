from math import ceil
f = open("inf_22_10_20_26 (4).txt")

a = f.read().splitlines()

a = a[1:]

a = list(map(int, a))
a.sort()

s = 0
sale = 0
last = 0

for i in range(len(a)):
    if a[i] <= 50:
        s += a[i]
        last = i

a = a[last + 1:]

flag = False
while len(a) != 0:
    for x in a:
        if not flag:
            s += a.pop()
            flag = True
        else:
            last = a.pop(0)
            sale += last
            flag = False

s += ceil(sale * 0.75)

print(s, last)