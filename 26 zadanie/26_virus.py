def get_second(x):
    return x[1]


f = open("26-68.txt")
a = f.read().splitlines()
n, t = map(int, a[0].split())
a = a[1:]

new = []
supervirus_danger = 0

for x in a:
    danger, time = map(int, x.split())
    supervirus_danger += danger
    new.append([danger, time])
supervirus_danger = supervirus_danger // n + 1
new.sort()

superviruses = []
regular = []
for x in new:
    if x[0] >= supervirus_danger:
        superviruses.append(x)
    else:
        regular.append(x)
superviruses.sort(key=get_second)
regular.sort(key=get_second)

count = 0
time_supervirus = 0
turn = 0
while t - superviruses[0][1] > 0 or t - regular[0][1] > 0:
    if turn % 2 == 0 and t - superviruses[0][1] - regular[0][1] >= 0:
        count += 1
        time_supervirus += superviruses[0][1]
        t -= superviruses[0][1]
        superviruses = superviruses[1:]
        turn += 1
    elif t - regular[0][1] >= 0:
        count += 1
        t -= regular[0][1]
        turn += 1
        regular = regular[1:]

print(count, time_supervirus)
