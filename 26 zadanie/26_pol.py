f = open("26-50.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))
a.sort()
half = a[len(a) // 2 - 1]
quarter = a[(len(a) // 4) * 3]
print(half, quarter)
print(a)
count = 0
min_sr = 1000000000000
for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        if (a[i] + a[j]) % 2 == 0:
            sum = a[i] + a[j]
            sr = sum // 2
            if half < sr < quarter:
                count += 1
                if sr < min_sr:
                    min_sr = sr
print(count, min_sr)
