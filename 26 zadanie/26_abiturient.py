f = open("26-60.txt")

a = f.read().splitlines()
k, n = map(int, a[0].split())
a = a[1:]

vuz = {x: [] for x in range(k)}
competition = {vuz: [0, 0] for vuz in range(k)}

for i in range(k):
    count = int(a[i])  # кол-во мест
    vuz[i] = [0 for x in range(count)]
    competition[i][0] = count

a = a[1000:]


for x in a:
    score, v = map(int, x.split())
    competition[v][1] += 1
    if vuz[v][0] < score:
        vuz[v][0] = score
        vuz[v].sort()

count = 0
for x in vuz:
    for score in vuz[x]:
        if score > 0:
            count += 1
print(count)

max_comp = -1
max_vuz = 0
for x in competition:
    if competition[x][1] / competition[x][0] > max_comp:
        max_comp = competition[x][1] / competition[x][0]
        max_vuz = x

print(vuz[max_vuz][0])
