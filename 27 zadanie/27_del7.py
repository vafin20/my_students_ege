f = open("27986_A.txt")

a = f.read().splitlines()

a = list(map(int, a))

max_7 = 0
max = 0

for x in a:
    if x % 7 == 0 and x % 49 != 0 and x > max_7:
        max_7 = x
    elif x % 7 != 0 and x % 49 != 0 and x > max:
        max = x

print(max * max_7)
