f = open()

a = f.read().splitlines()

min_dif_1 = 1000000000
min_dif_2 = 1000000000
sum = 0

for x in a:
    a, b = map(int, x.split())
    if a > b:
        sum += a
    else:
        sum += b
    dif = abs(a - b)
    if dif % 3 == 1 and dif < min_dif_1:
        min_dif_1 = dif
    elif dif % 3 == 2 and dif < min_dif_2:
        min_dif_2 = dif

if sum % 3 == 0:
    print(sum)
elif sum % 3 == 1:
    print(sum - min_dif_1)
else:
    print(sum - min_dif_2)