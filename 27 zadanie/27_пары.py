f = open()

a = f.read().splitlines()

a = a[1:]
s = 0
min_dif = 1000000000

for i in range(len(a)):
    x, y = map(int, a[i].split())
    if x > y:
        s += x
    else:
        s += y
    if min_dif > abs(x - y) > 0:
        min_dif = abs(x - y)

if s % 5 != 0:
    print(s)
else:
    print(s - min_dif)