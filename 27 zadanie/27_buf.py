f = open()
a = f.read().splitlines()[1:]
a = list(map(int, a))

buf = [a[0], a[1], a[2]]

min_sum = 10000000000
min_num = 10000000000

for i in range(3, len(a)):
    x = a[i]
    if x * x + buf[0] * buf[0] < min_sum:
        min_sum = x * x + buf[0] * buf[0]
        min_num = min(x, min_num)
    elif x * x + min_num * min_num < min_sum:
        min_sum = x * x + min_num * min_num
        min_num = min(x, min_num)
    for k in range(2):
        a[k] = a[k + 1]
    a[2] = x

print(min_sum)