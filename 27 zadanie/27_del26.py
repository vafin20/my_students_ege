f = open("27985_B.txt")

a = f.read().splitlines()

a = a[1:]

a = list(map(int, a))


max_13 = 0
max_2 = 0
max_26 = 0
max_global = 0

for x in a:
    if x % 13 == 0 and x > max_13 and x % 2 != 0:
        max_13 = x
    if x % 2 == 0 and x > max_2 and x % 13 != 0:
        max_2 = x
    if x % 26 == 0 and x > max_26:
        max_26 = x
    elif x > max_global:
        max_global = x

if max_global * max_26 > max_2 * max_13:
    print(max_global * max_26)
else:
    print(max_2 * max_13)
