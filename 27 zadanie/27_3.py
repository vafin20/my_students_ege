f = open("inf_22_10_20_27b (2).txt")
a = f.read().splitlines()[1:]


min_dif_1 = []
min_dif_2 = []
sum = 0

for x in a:
    k, m = map(int, x.split())  # 0 1 -> ["0", "1"] -> [0, 1] -> k = 0, m = 1
    sum += min(k, m)
    dif = abs(k - m)
    if dif % 3 == 1:
        min_dif_1.append(dif)
    elif dif % 3 == 2:
        min_dif_2.append(dif)

min_dif_2.sort()
min_dif_1.sort()
print(sum, sum % 3, min_dif_1[:5], min_dif_2[:5])