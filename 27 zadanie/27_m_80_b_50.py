"""https://inf-ege.sdamgia.ru/problem?id=28130"""

f = open("28130_B.txt")

a = f.read().splitlines()
a = a[1:]
a = list(map(int, a))
m = 80
b = 50

count_0 = []

for i in range(80):
    count_0.append(0)


count_1 = []

for i in range(80):
    count_1.append(0)

for x in a:
    if x > b:
        count_1[x % m] += 1
    else:
        count_0[x % m] += 1


sum = 0
# от 1 до 39
for i in range(1, 40):
    sum += count_0[i] * count_1[m - i] + count_1[i] * count_1[m - i] + count_1[i] * count_0[m - i]

# 0 и 40

sum += count_0[0] * count_1[0] + count_1[0] * (count_1[0] - 1) // 2
sum += count_0[40] * count_1[40] + count_1[40] * (count_1[40] - 1) // 2

print(sum)