f = open("27-B_demo (1).txt")

a = f.read().splitlines()

a = a[1:]

min_dif = 1000000
s = 0

for x in a:
    k, m = map(int, x.split())
    s += max(k, m)
    if abs(k - m) < min_dif and abs(k - m) % 5 != 0:
        min_dif = abs(k - m)

if s % 5 == 0:
    print(s - min_dif)
else:
    print(s)