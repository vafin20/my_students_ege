f = open("28130_B.txt")

a = f.read().splitlines()[1:]
a = list(map(int, a))

count = 0
m_more_50 = [0 for i in range(80)]  # > 50
m_less_50 = [0 for x in range(80)]  # <= 50

for x in a:
    d = x % 80
    if x > 50:
        m_more_50[d] += 1
    else:
        m_less_50[d] += 1

count += m_more_50[0] * m_less_50[0] + (m_more_50[0] * (m_more_50[0] - 1)) // 2
count += m_more_50[40] * m_less_50[40] + (m_more_50[40] * (m_more_50[40] - 1)) // 2

for i in range(1, 40):
    count += m_more_50[i] * m_more_50[80 - i]
    count += m_more_50[i] * m_less_50[80 - i]
    count += m_less_50[i] * m_more_50[80 - i]

print(count)