f = open("27-B (3).txt")
a = f.read().splitlines()[1:]

sum = 0
count_chet = 0
count_nechet = 0
all_difs = []

for x in a:
    k, l = map(int, x.split())
    max_num = max(k, l)
    sum += max_num
    if max_num % 2 == 0:
        count_chet += 1
    else:
        count_nechet += 1
    dif = abs(k - l)
    if dif != 0:
        all_difs.append(dif)
    if dif == 27 or dif == 47:
        print(k, l)

all_difs.sort()
print((sum - 74) % 2, count_nechet - 1 - 1, count_chet + 1 + 1)
print(all_difs)