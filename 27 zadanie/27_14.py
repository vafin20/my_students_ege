"""f = open("../27985_B.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))"""

a = [14, 7, 4, 28]
max_14 = a[0]
max_7 = 0
max_2 = 0
max_global = a[0]

for x in a:
    if x % 14 == 0 and max_14 < x < max_global:
        max_14 = x
    if x % 7 == 0 and x % 2 != 0 and x > max_7:
        max_7 = x
    if x % 7 != 0 and x % 2 == 0 and x > max_2:
        max_2 = x
    if x > max_global:
        max_global = x
print(max_14, max_7, max_global, max_2)