f = open("27-65b.txt")
a = f.read().splitlines()[1:]

pairs = []
max_nums = []
min_nums = []

for x in a:
    k, l = map(int, x.split())
    if l % 2 != 0:
        max_nums.append(max(k, l))
        min_nums.append(min(k, l))
        pairs.append([k, l])

pairs.sort(key=lambda pair: pair[0] + pair[1])
print(sum(max_nums))
print(sum(min_nums))
print(pairs[:5])
for x in pairs:
    if max(x) % 2 == 0 and min(x) % 2 == 1:
        print(x)
        break
