f = open("27-B (1).txt")
a = f.read().splitlines()[1:]

first = 0
second = 0
third = 0
min_dif = 100000000

for x in a:
    y = list(map(int, x.split()))
    y.sort()
    third += y[0]
    second += y[1]
    first += y[2]
    if y[2] - y[0] > y[1] - y[0] and 0 < y[1] - y[0] < min_dif:
        min_dif = y[1] - y[0]
    elif y[2] - y[0] < y[1] - y[0] and 0 < y[2] - y[0] < min_dif:
        min_dif = y[2] - y[0]


print(first % 2, second % 2, third + min_dif)
