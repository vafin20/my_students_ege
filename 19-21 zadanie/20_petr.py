"""
19 задание
x – 1 куча
p – чей ход
"""


def f(x, y, p):
    if x + y >= 88 and p == 3:  # Выигрывает Петька ВТОРЫМ ХОДОМ
        return True
    if x + y < 88 and p == 3:  # Петька идет нахуй вторым ходом
        return False
    if x + y >= 88:  # Либо Ваня выиграл первым ходом, либо Петя
        return False
    if p % 2 == 0:
        return f(x + 1, y, p + 1) or f(x * 3, y, p + 1) or f(x, y + 1, p + 1) or f(x, y * 3, p + 1)
    else:
        return f(x + 1, y, p + 1) and f(x, y + 1, p + 1) and f(x * 3, y, p + 1) and f(x, y * 3, p + 1)


for i in range(1, 72):  # Диапазон S
    if f(6, i, 0):  # 0 - ничей ход, 1 – Петя, 2 – Ваня, 3 – Петя и тд
        print(i)