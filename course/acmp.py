from math import ceil

f = open("inf_22_10_20_26 (3).txt")
a = f.read().splitlines()


s = int(a[0])
a = a[1:]
a = list(map(int, a))
a.sort()

sale = 0
sum = 0
last = 0
max_sale = 0
for i in range(0, len(a)):
    if a[i] <= 100:
        sum = sum + a[i]
        last = i

a = a[last + 1:]
flag = False
while len(a) != 0:
    if not flag:
        sum += a.pop()
        flag = True
    else:
        s = a.pop(0)
        sale += s
        max_sale = s
        flag = False
sale = ceil(sale * 0.70)
sum += sale
print(sum, max_sale)
