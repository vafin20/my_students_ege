a = set()


def f(x, k):
    if k == 9:
        a.add(x)
        return
    return f(x * 2, k + 1), f((x * 2) + 1, k + 1)


f(1, 0)
print(len(a))

