def simple(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True

def f(x):
    count = 0
    last_simple = 0
    for i in range(1, x + 1):
        if x % i == 0:
            count += 1
            if simple(i):
                last_simple = i
        if count > 128:
            return None
    if count == 128:
        return last_simple


for x in range(1, 1000000000):
    print(x)
    if f(x) is not None:
        print(x, f(x))
        break