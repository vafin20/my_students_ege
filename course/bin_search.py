def binary(arr, start, stop):
    mid = (start + stop) // 2

    if arr[mid] % 2 != 0:
        if arr[mid + 1] % 2 == 0:
            return mid + 1
        else:
            return binary(arr, mid + 2, stop)
    else:
        if arr[mid - 1] % 2 != 0:
            return mid
        else:
            return binary(arr, 0, mid - 2)


a = [1, 3, 5, 4, 4, 4]

print(binary(a, 0, len(a) - 1))
