f = open("inf_22_10_20_27b.txt")

a = f.read().splitlines()

a = a[1:]
s = 0
m = []

for b in a:
    x, y = map(int, b.split())
    s += max(x, y)
    if x != y and abs(x - y) % 3 != 0:
        d = abs(x - y)
        m.append(d)
m.sort()

if s % 3 == 0:
    print(s)
else:
    for x in m:
        if (s - x) % 3 == 0:
            print(s - x)
            break
