f = open("27_B (3).txt")

a = f.read().splitlines()[1:]
print(len(a))
a = list(map(int, a))

prefix_sum = [0 for i in range(len(a) + 1)]

for i in range(1, len(a) + 1):
    prefix_sum[i] = prefix_sum[i - 1] + (a[i - 1] % 43)

print(prefix_sum[12345:54322])
