def sort(a):
    for i in range(len(a)):
        for j in range(i + 1, len(a)):
            if a[j] < a[i]:
                temp = a[i]
                a[i] = a[j]
                a[j] = temp
    return a


n, s = map(int, input().split())

a = list(map(int, input().split()))

a = sort(a)

al = 0
count = 0
for x in a:
    if x + al <= s:
        al += x
        count += 1

print(count)