f = open("27990_B.txt")
a = f.read().splitlines()
a = a[1:]
a = list(map(int, a))


count_31 = 0
count_2 = 0
count_62 = 0
count = 0

for x in a:
    if x % 62 == 0:
        count_62 += 1
    elif x % 2 == 0:
        count_2 += 1
    elif x % 31 == 0:
        count_31 += 1
    else:
        count += 1

s = count_2 * count_31
p = count * count_62
r = count_62 * count_31
t = count_62 * count_2
y = (count_62 * (count_62 - 1)) // 2
print(s + p + r + t + y)