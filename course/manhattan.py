def f(x):
    a = 1
    b = x
    while b > 0:
        a *= b % 7
        b = b // 7
    return a

for x in range(10000):
    if f(x) == 40:
        print(x)
        break