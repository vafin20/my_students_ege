n = int(input())
a = list(map(int, input().split()))
tri = 0
chet = 0
first = []
second = []
for x in a:
    if x % 2 != 0:
        first.append(x)
        tri += 1
    else:
        second.append(x)
        chet += 1


s = ""
for x in first:
    s += str(x) + " "
s = s[:len(s) - 1]
print(s)

s1 = ""
for x in second:
    s1 += str(x) + " "
s1 = s1[:len(s1) - 1]
print(s1)
if chet > tri:
    print("YES")
else:
    print("NO")
