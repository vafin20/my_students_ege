"""
Светлана составляет коды из букв слова РОСОМАХА.
Код должен состоять из 8 букв,
и каждая буква в нём должна встречаться столько же раз,
сколько в заданном слове. Кроме того,
в коде не должны стоять рядом две гласные и две согласные буквы.
Сколько кодов может составить Светлана?
"""
import itertools


def valid(x):
    sogl = ["р", "с", "м", "х"]
    gl = ["о", "а"]
    for i in range(len(x) - 1):
        if (x[i] in sogl and x[i + 1] in sogl) or (x[i] in gl and x[i + 1] in gl):
            return False
    return True


s = "росомаха"


res = set()

for x in itertools.permutations(s):
    a = ''.join(x)
    if valid(a):
        res.add(a)

print(len(res))
