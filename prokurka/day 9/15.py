"""
ДЕЛ(A, 7) ∧ (ДЕЛ(240, x) → (¬ДЕЛ(A, x) → ¬ДЕЛ(780, x)))
"""


def dell(x, a):
    return x % a == 0


for a in range(1, 1000):
    count = 0
    for x in range(1, 1000):
        if dell(a, 7) and ((dell(240, x)) <= ((not dell(a, x)) <= (not(dell(780, x))))):
            count += 1
        if count == 999:
            print(a)
            break
