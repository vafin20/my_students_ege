f = open("27-B (11).txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_sum = -1
sum = 0
mins = [0 for i in range(10)]
k = 0
for x in a:
    sum += x
    if x % 2 == 0:
        k += 1
    ost = k % 10
    if ost == 0:
        max_sum = max(max_sum, sum)

    if mins[ost] == 0:
        mins[ost] = sum
    else:
        if sum - mins[ost] > max_sum:
            max_sum = sum - mins[ost]

print(max_sum)