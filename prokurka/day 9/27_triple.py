f = open()
a = f.read().splitlines()[1:]

s = 0
min_dif = 100000000

for x in a:
    k, l, m = map(int, x.split())
    s += max(k, l, m)
    difs = [abs(k - l), abs(k - m), abs(l - m)]
    for dif in difs:
        if dif % 109 != 0 and dif < min_dif:
            min_dif = dif
if s % 109 == 0:
    print(s - min_dif)
else:
    print(s)
