f = open("27_B (9).txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_sum = -1
min_len = 100000000
mins = [[10001, 0] for i in range(43)]
s = 0

for i in range(len(a)):
    s += a[i]
    ost = s % 43

    if ost == 0:
        max_sum = s
        min_len = i + 1

    if mins[ost][0] == 10001:
        mins[ost][0] = s
        mins[ost][1] = i
    else:
        if s - mins[ost][0] > max_sum:
            max_sum = s - mins[ost][0]
            min_len = i - mins[ost][1]
        elif s - mins[ost][0] == max_sum:
            min_len = min(min_len, s - mins[ost][1])

print(max_sum)