import re

for x in range(10**8):
    s = str(x)
    if re.fullmatch("1234\d7", s) and x % 141 == 0:
        print(x, x // 141)