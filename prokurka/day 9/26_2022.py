f = open("26_2022.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))
a.sort(reverse=True)

count = 1
last = a[0]

for i in range(1, len(a)):
    if last - 3 >= a[i]:
        count += 1
        last = a[i]

print(count)