"""
+ 2, +10
7 -> 121
"""
a = [0 for i in range(122)]
a[7] = 1

for i in range(9, 122):
    a[i] = a[i - 2] + a[i - 10]

print(a)