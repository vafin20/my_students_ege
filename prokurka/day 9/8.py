import itertools

s = sorted("цапля")
index = 1
for x in itertools.product(s, repeat=5):
    a = ''.join(x)
    if a.count("а") <= 1 and a.count("ц") == 2 and a.count("л") == 0:
        print(index, a)
    index += 1
