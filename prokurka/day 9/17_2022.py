f = open()
a = f.read().splitlines()
a = list(map(int, a))

count = 0
max_sum = -1
min_elem = min(a)

for i in range(len(a) - 1):
    if a[i] % 117 == min_elem or a[i + 1] % 117 == min_elem:
        count += 1
        if a[i] + a[i + 1] > max_sum:
            max_sum = a[i] + a[i + 1]

print(count, max_sum)