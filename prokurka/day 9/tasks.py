def f(x):
    d = []
    for i in range(2, x):
        if x % i == 0:
            d.append(i)
    if len(d) != 0:
        s = max(d) + min(d)
        if s % 138 == 0:
            return s

for x in range(800001, 100000000):
    if f(x):
        print(x, f(x))