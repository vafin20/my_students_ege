f = open("28128_B.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_sum = -1
first = 0
second = 0

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        if a[i] % 160 != a[j] % 160 and (a[i] % 7 == 0 or a[j] % 7 == 0):
            s = a[i] + a[j]
            if s > max_sum:
                first = a[i]
                second = a[j]
                max_sum = s

print(first, second)