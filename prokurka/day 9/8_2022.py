import itertools

s = "012345"
count = 0

for x in itertools.product(s, repeat=5):
    a = ''.join(x)
    for i in range(len(a) - 1):
        if a[0] != "0" and ((a[i] in "024" and a[i + 1] in "135") or (a[i] in "135" and a[i + 1] in "024")):
            count += 1

print(count)
