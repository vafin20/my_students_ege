from math import ceil

f = open("27.txt")
a = f.read().splitlines()
default = [[0, 0] for i in range(len(a))]
b = [[0, 0] for i in range(len(a))]
mins = [[0, 0] for i in range(len(a))]

for i in range(len(a)):
    dist, count = map(int, a[i].split())
    default[i] = [dist, count]
    count = ceil(count / 36)
    b[i] = [dist, count]

mins[0][0] = b[0][0]
for x in b:
    mins[0][1] += (x[0] - mins[0][0]) * x[1]

pre = b[0][1]
next = 0
for i in range(1, len(a)):
    next += b[i][1]

for i in range(1, len(a)):
    mins[i][0] = b[i][0]
    delta = b[i][0] - b[i - 1][0]
    mins[i][1] = mins[i - 1][1] + delta * pre - delta * next
    pre += b[i][1]
    next -= b[i][1]

min_index = 0
min_cost = 100000000

for x in mins:
    if x[1] < min_cost:
        min_cost = x[1]
        min_index = x[0]

print(min_index)

for x in default:
    if x[0] == min_index:
        print(x)