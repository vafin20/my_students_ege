f = open("24_demo.txt")
a = f.read()

count = 0
max_count = -1

for i in range(len(a) - 1):
    if a[i] == "X":
        count += 1
        if count > max_count:
            max_count = count
    else:
        count = 0

print(max_count)