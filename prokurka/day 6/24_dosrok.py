f = open("107_24.txt")
a = f.read()
a = a.replace("AB", "*")
a = a.replace("CB", "*")

count = 0
max_count = 0

for x in a:
    if x == "*":
        count += 1
        if count > max_count:
            max_count = count
    else:
        count = 0

print(max_count)