f = open()
a = f.read().splitlines()

new = []
for x in a:
    if x.count("A") < 25:
        new.append(x)

max_dist = -1

for string in new:
    for i in range(len(string)):
        if string.count(string[i]) > 1:
            dist = string.rfind(string[i]) - i
            if dist > max_dist:
                max_dist = dist

print(max_dist)