"""
 0 - НИКТО
 1 - Петя
 2 - Ваня
 3 - Петя
"""


def f(x, p, ban):
    if x >= 50 and p == 2:
        return True
    if x < 50 and p == 2:
        return False
    if x >= 50:
        return False
    if p % 2 == 0:
        if ban == 0:
            return f(x + 1, p + 1, 1) and f(x + 4, p + 1, 2) and f(x * 2, p + 1, 3)
        elif ban == 1:
            return f(x + 4, p + 1, 2) and f(x * 2, p + 1, 3)
        elif ban == 2:
            return f(x + 1, p + 1, 1) and f(x * 2, p + 1, 3)
        elif ban == 3:
            return f(x + 1, p + 1, 1) and f(x + 4, p + 1, 2)
    else:
        if ban == 0:
            return f(x + 1, p + 1, 1) or f(x + 4, p + 1, 2) or f(x * 2, p + 1, 3)
        elif ban == 1:
            return f(x + 4, p + 1, 2) or f(x * 2, p + 1, 3)
        elif ban == 2:
            return f(x + 1, p + 1, 1) or f(x * 2, p + 1, 3)
        elif ban == 3:
            return f(x + 1, p + 1, 1) or f(x + 4, p + 1, 2)


for s in range(1, 50):
    if f(s, 0, 0):
        print(s)