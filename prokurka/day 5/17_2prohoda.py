f = open("17 (18).txt")
a = f.read().splitlines()
a = list(map(int, a))

chet = []
for x in a:
    if x % 2 == 0:
        chet.append(x)
sr = sum(chet) / len(chet)

count = 0
max_sum = -1000000
for i in range(len(a) - 1):
    if (a[i] % 3 == 0 or a[i + 1] % 3 == 0) and (a[i] < sr or a[i + 1] < sr):
        count += 1
        if a[i] + a[i + 1] > max_sum:
            max_sum = a[i] + a[i + 1]

print(count, max_sum)