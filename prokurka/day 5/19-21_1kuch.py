def f(x, p):
    if x >= 68 and p == 2:
        return True
    if x < 68 and p == 2:
        return False
    return f(x + 1, p + 1) or f(x + 4, p + 1) or f(x * 5, p + 1)


for s in range(1, 68):
    if f(s, 0):
        print(s)