"""
+1, *2
>= 86
1 <= s <= 71
0 - no
1 - pet
2 - van
"""


def f(x, y, p):
    if x + y >= 86 and (p == 2 or p == 4):
        return True
    if x + y < 86 and p == 4:
        return False
    if x + y >= 86:
        return False
    if p % 2 == 1:
        return f(x + 1, y, p + 1) or f(x, y + 1, p + 1) or f(x * 2, y, p + 1) or f(x, y * 2, p + 1)
    else:
        return f(x + 1, y, p + 1) and f(x, y + 1, p + 1) and f(x * 2, y, p + 1) and f(x, y * 2, p + 1)


for s in range(1, 72):
    if f(14, s, 0):
        print(s)