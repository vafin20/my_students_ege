f = open("26 (32).txt")
a = f.read().splitlines()[1:]

marks = {i: set() for i in range(1961, 1992)}

for x in a:
    year, type = map(int, x.split())
    marks[year].add(type)

max_count = -1
max_year = -1
count = 0
print(marks)
for year in range(1961, 1992):
    k = 8 - len(marks[year])
    count += k
    if k >= max_count:
        max_count = k
        max_year = year

print(count, max_year)
