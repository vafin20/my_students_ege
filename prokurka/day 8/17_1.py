f = open("RYH1r9GOG.txt")
a = f.read().splitlines()
a = list(map(int, a))

count = 0
min_103 = 100000000

for x in a:
    if x < min_103 and x % 103 == 0:
        min_103 = x

max_sum = -1

for i in range(len(a) - 1):
    sum = a[i] + a[i + 1]
    if sum % 2 == 0:
        dif = abs(a[i] - a[i + 1])
        if dif % min_103 == 0:
            count += 1
            if sum > max_sum:
                max_sum = sum

print(count, max_sum)