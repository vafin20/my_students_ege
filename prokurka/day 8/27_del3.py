f = open("inf_22_10_20_27b (3).txt")
a = f.read().splitlines()[1:]

sum = 0
dif_1 = []
dif_2 = []
dif_3 = []

for x in a:
    a, b = map(int, x.split())
    sum += min(a, b)
    dif = abs(a - b)
    if dif % 3 == 1:
        dif_1.append(dif)
    elif dif % 3 == 2:
        dif_2.append(dif)
    elif dif != 0:
        dif_3.append(dif)
dif_2.sort()
dif_1.sort()
dif_3.sort()

print(sum, sum % 3)
print(dif_1[:5])
print(dif_2[:5])
print(dif_3[:5])