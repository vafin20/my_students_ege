f = open("26 (31).txt")
a = f.read().splitlines()
k, n = map(int, a[0].split())
a = a[1:]

vuz = {i: [] for i in range(k)}
competition = {i: [] for i in range(k)}

for i in range(k):
    slots = int(a[i])
    vuz[i] = [0 for i in range(slots)]
    competition[i] = [slots, 0]

a = a[1000:]

for x in a:
    mark, id = map(int, x.split())
    competition[id][1] += 1
    if vuz[id][0] < mark:
        vuz[id][0] = mark
        vuz[id].sort()

max_comp = -1
max_mark = -1

for i in range(len(competition)):
    comp = competition[i][1] / competition[i][0]
    if comp > max_comp:
        max_comp = comp
        max_mark = vuz[i][0]

count = 0

for id in vuz:
    for mark in vuz[id]:
        if mark > 0:
            count += 1

print(count, max_mark)