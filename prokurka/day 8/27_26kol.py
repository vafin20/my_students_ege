f = open("27989_B.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))
n = len(a)

c_2 = 0
c_13 = 0
c_26 = 0


for x in a:
    if x % 26 == 0:
        c_26 += 1
    elif x % 13 == 0 and x % 2 != 0:
        c_13 += 1
    elif x % 13 != 0 and x % 2 == 0:
        c_2 += 1


s = c_13 * c_2
s += c_26 * (n - c_26)
s += (c_26 * (c_26 - 1)) // 2

print(s)