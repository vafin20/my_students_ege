import math


def f(x):
    d = set()
    for i in range(1, int(math.sqrt(x)) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
    s = sum(d)
    p = math.prod(d)
    if s % 2 == 1 and p % 2 == 1 and len(d) > 10:
        return len(d)


for x in range(800001, 10000000000):
    if f(x):
        print(x, f(x))