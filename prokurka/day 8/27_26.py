f = open("27988_B.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_2 = -1
max_13 = -1
max_26 = -1
m = -1


for x in a:
    if x % 26 == 0 and x > max_26:
        max_26 = x
    elif x % 13 == 0 and x % 2 != 0 and x > max_13:
        max_13 = x
    elif x % 2 == 0 and x % 13 != 0 and x > max_2:
        max_2 = x
    elif x > m:
        m = x

print(max(max_2 * max_13, max_26 * m))