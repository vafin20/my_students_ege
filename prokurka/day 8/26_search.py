f = open("inf_26_04_21_26 (4).txt")
a = f.read().splitlines()
a = a[1:]
a = list(map(int, a))

count = 0
max_sum = -1
sums = set(a)

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        if a[i] % 2 == a[j] % 2:
            sum = a[i] + a[j]
            if sum in sums:
                count += 1
                if sum > max_sum:
                    max_sum = sum

print(count, max_sum)
