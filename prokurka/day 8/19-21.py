def f(x, y, p):
    if x + y >= 231 and (p == 2 or p == 4):
        return True
    if x + y < 231 and p == 4:
        return False
    if x + y >= 231:
        return False
    if p % 2 == 1:
        return f(x + 2, y, p + 1) or f(x * 2, y, p + 1) or f(x, y + 2, p + 1) or f(x, y * 2, p + 1)
    else:
        return f(x + 2, y, p + 1) and f(x * 2, y, p + 1) and f(x, y + 2, p + 1) and f(x, y * 2, p + 1)


for s in range(1, 214):
    if f(17, s, 0):
        print(s)