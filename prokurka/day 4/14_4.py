for i in range(1, 1000):
    x = 216**5 + 6**3 - 1 - i
    s = ""
    while x > 0:
        s += str(x % 6)
        x //= 6
    if s.count("5") == 12:
        print(i)
        break