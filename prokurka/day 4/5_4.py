def f(x):
    s = 255 ^ x
    return s - x


for x in range(10000):
    if f(x) == 111:
        print(x)
        break