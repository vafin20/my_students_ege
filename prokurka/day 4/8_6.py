import itertools

s = "руслан"

count = 0
for x in itertools.permutations(s):
    a = ''.join(x)
    if "уа" not in a and "ау" not in a:
        count += 1

print(count)