from math import sqrt


def f(x):
    d = set()
    m = 0
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0:
            d.add(i)
            d.add(x // i)
    if len(d) != 0:
        m = max(d) + min(d)
    if m % 7 == 3:
        return m


for x in range(452022, 10000000000):
    if f(x):
        print(x, f(x))
