def f(x):
    for i in range(2, x):
        if x % i == 0 and i != 7 and i % 10 == 7:
            return i


for x in range(600001, 100000000):
    if f(x):
        print(x, f(x))