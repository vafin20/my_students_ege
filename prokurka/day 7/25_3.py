def f(x):
    d = []
    for i in range(2, x):
        if x % i == 0:
            d.append(i)
    if len(d) == 4:
        return d


for x in range(210235, 210300 + 1):
    if f(x):
        print(f(x))
