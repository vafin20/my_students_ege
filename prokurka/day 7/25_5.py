def count(x):
    count = 0
    for i in range(1, x + 1):
        if x % i == 0:
            count += 1
    return count

max_count = -1
max_num = -1
for x in range(84052, 84131):
    if count(x) > max_count:
        max_count = count(x)
        max_num = x

print(max_count, max_num)