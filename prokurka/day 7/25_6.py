from math import sqrt


def f(x):
    d = []
    if sqrt(x) == int(sqrt(x)):
        for i in range(2, int(sqrt(x))):
            if x % i == 0:
                if len(d) != 0:
                    return False
                else:
                    d.append(i)
                    d.append(x // i)
        if len(d) != 0:
            return max(d)
    else:
        return False


for x in range(123456789, 223456789 + 1):
    if f(x):
        print(x, f(x))
