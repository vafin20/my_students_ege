def f(x):
    n = 0
    m = 0
    while x % 2 == 0:
        m += 1
        x //= 2
    while x % 3 == 0:
        n += 1
        x //= 3
    if x == 1 and m % 2 == 0 and n % 2 == 1:
        return True
    else:
        return False


for x in range(200_000_000, 400_000_000 + 1):
    if f(x):
        print(x)