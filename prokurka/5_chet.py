def f(x):
    s1 = 0
    s2 = 0
    s = str(x)
    for i in range(len(s)):
        if int(s[i]) % 2 == 0:
            s1 += int(s[i])
        if (i + 1) % 2 == 0:
            s2 += int(s[i])
    return abs(s2 - s1)




for x in range(2, 10000):
    if f(x) == 13:
        print(x)
        break