import itertools

s = "01234567"

count = 0
for x in itertools.product(s, repeat=5):
    a = ''.join(x)
    if a[0] != "0" and a.count("6") == 1 and "16" not in a and "61" not in a and "36" not in a and "63" not in a and "56" not in a and "65" not in a and "67" not in a and "76" not in a:
        count += 1


print(count)