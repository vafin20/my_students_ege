from math import sqrt


def f(x):
    if sqrt(x) == int(sqrt(x)):
        count = 1
        for i in range(2, int(sqrt(x))):
            if x % i == 0:
                last = x // i
                count += 2
        if count == 3:
            return last


for x in range(289123456, 389123456):
    if f(x) is not None:
        print(x, f(x))