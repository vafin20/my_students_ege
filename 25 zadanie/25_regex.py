import re
from math import sqrt


def simple(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True


def f(x):
    d = []
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0 and simple(i):
            d.append(i)
            if simple(x // i):
                d.append(x // i)
    d.sort()
    s = ""
    for x in d:
        s += str(x)
    if re.match("27\d*39\d", s) or re.match("34\d*2\d7", s):
        return d[-1]


for x in range(4679001, 10_000_000):
    if f(x):
        print(x, f(x))