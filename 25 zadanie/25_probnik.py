from math import sqrt

def simple(x):
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0:
            return False
    return True


def f(x):
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0:
            pair = x // i
            if simple(i) and simple(pair) and pair % 10 == i % 10:
                return True
    return False


valid = []
for x in range(264871, 322989 + 1):
    if f(x):
        valid.append(x)

print(len(valid))
print(sum(valid)/len(valid))
