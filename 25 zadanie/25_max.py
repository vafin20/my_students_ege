'''
Напишите программу, которая ищет среди целых чисел, принадлежащих числовому отрезку [84052; 84130],
 число, имеющее максимальное количество различных натуральных делителей,
 если таких чисел несколько — найдите минимальное из них.
 Выведите на экран количество делителей такого числа и само число.
'''

def f(x):
    count = 0
    for i in range(1, x + 1):
        if x % i == 0:
            count += 1
    return count


max_number = 0
max_count = 0
for x in range(84052, 84131):
    if f(x) > max_count: # f(x) - количество делителей
        max_count = f(x)
        max_number = x
print(max_count, max_number)