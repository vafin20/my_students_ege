def f(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True

s = 0
for x in range(1060, 18814):
    if f(x) == True:
        s += x
print(s)
