from math import sqrt


def f(x):
    count = 0
    max_trivia = 0
    if sqrt(x).is_integer():
        for n in range(2, int(sqrt(x))):
            if x % n == 0:
                count += 2
                max_trivia = x // n
            if count > 2:
                break
    if count == 2:
        return max_trivia


for x in range(123456789, 223456789 + 1):
    if f(x):
        print(x, f(x))
