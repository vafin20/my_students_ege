from math import sqrt

def f(x):
    count = 0
    for i in range(1, x + 1):
        if x % i == 0 and i % 2 != 0:
            count += 1
            pair = x // i
            if pair != i and pair % 2 != 0:
                count += 1
        if count > 5:
            return False
    if count == 5:
        return True
    else:
        return False

for x in range(35000000, 40000000):
    print(x)
    if f(x):
        print(x)