import collections
import math
from functools import lru_cache


@lru_cache()
def simple(x):
    for i in range(2, int(math.sqrt(x)) + 1):
        if x % i == 0:
            return False
    return True


@lru_cache()
def is_palindrome(x):
    reverse = str(x)[::-1]
    p = 1
    if simple(x) and reverse == str(x):
        while x > 0:
            p *= x % 10
            x //= 10
        return p
    else:
        return False


d = []

for x in range(100, 1_000_000_000):
    print(x)
    if is_palindrome(x):
        d.append(is_palindrome(x))
print(collections.Counter(d))
