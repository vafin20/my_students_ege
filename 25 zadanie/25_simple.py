from math import sqrt


def simple(x):
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0:
            return False
    return True


def f(x):
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0 and simple(i) and simple(x // i) and int(sqrt(x)) != sqrt(x):
            return True
    return False


s = []
for x in range(351627, 428763 + 1):
    if f(x):
        s.append(x)

print(len(s), sum(s)/len(s))