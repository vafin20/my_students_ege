"""
https://inf-ege.sdamgia.ru/problem?id=33197
"""
from math import sqrt

def f(x):
    count = []
    count_100 = 0
    for i in range(1, int(sqrt(x)) + 1):
        if x % i == 0:
            pair = x // i
            answer = abs(pair - i)
            count.append(answer)
            if answer <= 100:
                count_100 += 1
    if count_100 >= 3:
        return True
    else:
        return False


for x in range(1000000, 2000001):
    if f(x):
        print(x)