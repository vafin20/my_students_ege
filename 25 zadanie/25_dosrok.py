"""
12345?7*8
"""

import re

for x in range(1000000, 10**9):
    s = str(x)
    if re.match("12345\d7\d8", s) is not None and x % 23 == 0:
        print(x, x // 23)
# 1297908


