def func(x):
    count = []
    for i in range(1, x + 1):
        if x % i == 0:
            count.append(i)
    if len(count) == 4:
        return count[2], count[3]
    else:
        return None


for x in range(126849, 126872):
    if func(x) is not None:
        print(func(x))