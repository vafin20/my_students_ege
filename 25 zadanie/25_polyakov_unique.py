def f(x):
    if (x // 10000) % 2 != 0 and ((x % 10000) // 1000) % 2 != 0:
        if x % 7 != 0 and x % 9 != 0 and x % 13 != 0:
            return True
    else:
        return False


min_number = 10000000
max_number = 0
count = 0
for x in range(57888, 74556):
    if f(x):
        count += 1
        if x < min_number:
            min_number = x
        if x > max_number:
            max_number = x

print(count, max_number - min_number)

