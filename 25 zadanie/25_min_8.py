def f(x):
    for n in range(2, x):
        if x % n == 0 and n % 10 == 8 and n != 8:
            return n


for x in range(500001, 100000000):
    if f(x):
        print(x, f(x))