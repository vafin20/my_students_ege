from math import sqrt


def f(x):
    a = []
    count_100 = 0
    for n in range(1, int(sqrt(x)) + 1):
        if x % n == 0:
            pair = x // n
            dif = abs(pair - n)
            a.append(dif)
            if dif <= 100:
                count_100 += 1
    if count_100 >= 3:
        return True


for x in range(1000000, 2000001):
    if f(x):
        print(x)

