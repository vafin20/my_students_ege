def count(x):
    div = []
    for i in range(2, x):
        if x % i == 0:
            div.append(i)
    if len(div) == 4:
        return div


for x in range(210235, 210300 + 1):
    if count(x):
        print(count(x))