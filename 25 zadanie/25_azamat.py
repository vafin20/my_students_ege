"""
найдите те, которые делятся нацело на произведение своих ненулевых цифр
"""


def f(x):
    b = x
    p = 1
    while b > 0:
        if b % 10 != 0:
            p *= b % 10
            b //= 10
        else:
            b //= 10
    if x % p == 0:
        return p
    else:
        return None


for x in range(121212, 121490):
    if f(x) is not None:
        print(x, f(x))

