def f(n):
    x = n
    a = 0
    b = 1
    while x > 0:
        a = a + 1
        b = b * (x % 100)
        x = x // 100
    return(a, b)
for x in reversed(range (0, 100000)):
    if f(x) == (2, 5):
        print(x)
        break