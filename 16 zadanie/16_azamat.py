def f(n):
    if n == 6:
        return -1
    if n <= 5:
        return n
    elif n > 5 and n % 5 == 0:
        return n + f(n / 5 + 1)
    elif n > 5 and n % 5 != 0:
        return n + f(n + 6)


for n in range(1000):
    if f(n) is not None and f(n) > 1000:
        print(n)
        break