"""https://inf-ege.sdamgia.ru/problem?id=35475"""

from math import sqrt


def simple(x):
    for i in range(2, int(sqrt(x) + 1)):
        if x % i == 0:
            return False
    return True

def f(x):
    count = 0
    for i in range(2, x): # проверяю все делители
        if x % i == 0 and simple(i):
            count += 1 # + простой делитель в копилку
            if count > 3:
                return False
    if count == 3:
        return True
    else:
        return False


count = 0
min_number = 10000000
for x in range(10001, 50001):
    if f(x): # если подходит
        count += 1
        if x < min_number:
            min_number = x
print(count, min_number)