"""

Определите количество принадлежащих отрезку [2 · 1010; 4 · 1010] натуральных чисел,
которые делятся на 7 и на 100000 и при этом не делятся на 13, 29, 43 и 101
"""


count = 0
min_num = 10000000000
for x in range(11000, 22001):
    count_del = 0
    if x % 11 == 0:
        count_del += 1
    if x % 13 == 0:
        count_del += 1
    if x % 17 == 0:
        count_del += 1
    if x % 19 == 0:
        count_del += 1
    if count_del == 2:
        count += 1
        if x < min_num:
            min_num = x
print(count, min_num)